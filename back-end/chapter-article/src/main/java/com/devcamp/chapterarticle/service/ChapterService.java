package com.devcamp.chapterarticle.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.chapterarticle.model.Article;
import com.devcamp.chapterarticle.model.Chapter;
import com.devcamp.chapterarticle.repository.IChapterRepository;

@Service
public class ChapterService {
  @Autowired
  private IChapterRepository pIChapterRepository;

  public ArrayList<Chapter> getAllChapters() {
    ArrayList<Chapter> chapters = new ArrayList<>();
    pIChapterRepository.findAll().forEach(chapters::add);
    return chapters;
  }

  public Chapter createChapter(Chapter cChapter) {
    try {
      Chapter savedRole = pIChapterRepository.save(cChapter);
      return savedRole;
    } catch (Exception e) {
      return null;
    }
  }

  public Set<Article> getArticleByChapterId(long id) {
    Chapter vChapter = pIChapterRepository.findById(id);
    if (vChapter != null) {
      return vChapter.getArticles();
    } else
      return null;
  }
}
