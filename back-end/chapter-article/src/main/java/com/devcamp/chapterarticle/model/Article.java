package com.devcamp.chapterarticle.model;

import javax.persistence.*;
import java.util.*;
import javax.validation.constraints.*;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "article")
public class Article {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotEmpty(message = "You should fill article code!")
  @Size(min = 2, message = "Article code must be 2 characters!")
  @Column(name = "article_code", unique = true)
  private String articleCode;

  @NotNull(message = "You should fill article name!")
  @Size(min = 2, message = "Article name must be 2 characters!")
  @Column(name = "article_name")
  private String articleName;

  @NotNull(message = "You should fill article introduction!")
  @Size(min = 2, message = "Article introduction must be 2 characters!")
  @Column(name = "article_intro")
  private String articleIntro;

  @NotNull(message = "You should fill article page!")
  @Range(min = 1, message = "Must fill value greater than 1")
  @Column(name = "article_page")
  private Long articlePage;

  @ManyToOne(fetch = FetchType.LAZY)
  @JsonIgnore
  private Chapter chapter;

  public Article() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getArticleCode() {
    return articleCode;
  }

  public void setArticleCode(String articleCode) {
    this.articleCode = articleCode;
  }

  public String getArticleName() {
    return articleName;
  }

  public void setArticleName(String articleName) {
    this.articleName = articleName;
  }

  public String getArticleIntro() {
    return articleIntro;
  }

  public void setArticleIntro(String articleIntro) {
    this.articleIntro = articleIntro;
  }

  public Long getArticlePage() {
    return articlePage;
  }

  public void setArticlePage(Long articlePage) {
    this.articlePage = articlePage;
  }

  public Chapter getChapter() {
    return chapter;
  }

  public void setChapter(Chapter chapter) {
    this.chapter = chapter;
  }

}
