package com.devcamp.chapterarticle.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.chapterarticle.model.Article;
import com.devcamp.chapterarticle.model.Chapter;
import com.devcamp.chapterarticle.repository.IArticleRepository;
import com.devcamp.chapterarticle.repository.IChapterRepository;
import com.devcamp.chapterarticle.service.ArticleService;
import com.devcamp.chapterarticle.service.ChapterService;

@RestController
@RequestMapping("/article")
@CrossOrigin
public class ArticleController {
  @Autowired
  IArticleRepository pIArticleRepository;
  @Autowired
  IChapterRepository pIChapterRepository;
  @Autowired
  ArticleService articleService;
  @Autowired
  ChapterService chapterService;

  // get all article list
  @GetMapping("/all")
  public ResponseEntity<List<Article>> getAllArticles() {
    try {
      return new ResponseEntity<>(articleService.getAllArticles(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // get article by chapter id
  @GetMapping("/search/chapterId={id}")
  public ResponseEntity<Set<Article>> getArticleByChapterId(@PathVariable(name = "id") long id) {
    try {
      Set<Article> chapterArticle = chapterService.getArticleByChapterId(id);
      if (chapterArticle != null) {
        return new ResponseEntity<>(chapterArticle, HttpStatus.OK);
      } else
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // get article detail by id
  @GetMapping("/detail/{id}")
  public ResponseEntity<Object> getArticleById(@PathVariable(name = "id", required = true) Long id) {
    Optional<Article> article = pIArticleRepository.findById(id);
    if (article.isPresent()) {
      return new ResponseEntity<>(article, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // create new article by chapter
  @PostMapping("/create/{id}")
  public ResponseEntity<Object> createArticle(@PathVariable("id") int id, @Valid @RequestBody Article newArticle) {
    try {
      Chapter chapter = pIChapterRepository.findById(id);
      if (chapter != null) {
        Article article = new Article();
        article.setArticleCode(newArticle.getArticleCode());
        article.setArticleName(newArticle.getArticleName());
        article.setArticleIntro(newArticle.getArticleIntro());
        article.setArticlePage(newArticle.getArticlePage());
        article.setChapter(chapter);
        Article _article = pIArticleRepository.save(article);
        return new ResponseEntity<>(_article, HttpStatus.CREATED);
      } else
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // update article
  @PutMapping("/update/{chapterId}/{articleId}")
  public ResponseEntity<Object> updateArticle(@PathVariable(value = "chapterId") long chapterId,
      @PathVariable(value = "articleId") long articleId,
      @Valid @RequestBody Article updateArticle) {
    try {
      Chapter chapter = pIChapterRepository.findById(chapterId);
      Article articleData = pIArticleRepository.findById(articleId);
      Article article = articleData;
      article.setArticleCode(updateArticle.getArticleCode());
      article.setArticleName(updateArticle.getArticleName());
      article.setArticleIntro(updateArticle.getArticleIntro());
      article.setArticlePage(updateArticle.getArticlePage());
      article.setChapter(chapter);
      try {
        return new ResponseEntity<>(pIArticleRepository.save(article), HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Article: " +
                e.getCause().getCause().getMessage());
      }
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // delete article by id
  @DeleteMapping("/delete/{id}")
  public ResponseEntity<Article> deleteArticle(@PathVariable("id") long id) {
    try {
      pIArticleRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
