package com.devcamp.chapterarticle.repository;

import java.util.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.chapterarticle.model.Article;

@Repository
public interface IArticleRepository extends JpaRepository<Article, Long> {
  Article findById(long id);
}
