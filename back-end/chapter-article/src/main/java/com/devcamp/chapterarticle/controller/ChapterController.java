package com.devcamp.chapterarticle.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.chapterarticle.model.Chapter;
import com.devcamp.chapterarticle.repository.IChapterRepository;
import com.devcamp.chapterarticle.service.ChapterService;

@RestController
@RequestMapping("/chapter")
@CrossOrigin
public class ChapterController {
  @Autowired
  IChapterRepository pIChapterRepository;
  @Autowired
  ChapterService chapterService;

  // get all chapter list
  @GetMapping("/all")
  public ResponseEntity<List<Chapter>> getAllChapters() {
    try {
      return new ResponseEntity<>(chapterService.getAllChapters(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // get chapter detail by id
  @GetMapping("/detail/{id}")
  public ResponseEntity<Object> getChapterById(@PathVariable(name = "id", required = true) Long id) {
    Optional<Chapter> chapter = pIChapterRepository.findById(id);
    if (chapter.isPresent()) {
      return new ResponseEntity<>(chapter, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // create new chapter
  @PostMapping("/create")
  public ResponseEntity<Object> createUser(@Valid @RequestBody Chapter pChapter) {
    try {
      Chapter _chapter = pIChapterRepository.save(pChapter);
      return new ResponseEntity<>(_chapter, HttpStatus.CREATED);
    } catch (Exception e) {
      System.out.println("+++++++++++++++++++++:::::" + e.getCause().getCause().getMessage());
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified Chapter: " + e.getCause().getCause().getMessage());
    }
  }

  // update chapter
  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateChapter(@PathVariable("id") long id, @Valid @RequestBody Chapter pChapter) {
    try {
      Chapter chapterData = pIChapterRepository.findById(id);
      Chapter chapter = chapterData;
      chapter.setChapterCode(pChapter.getChapterCode());
      chapter.setChapterName(pChapter.getChapterName());
      chapter.setChapterIntro(pChapter.getChapterIntro());
      chapter.setChapterPage(pChapter.getChapterPage());
      chapter.setArticles(pChapter.getArticles());
      try {
        return new ResponseEntity<>(pIChapterRepository.save(chapter), HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Chapter: " +
                e.getCause().getCause().getMessage());
      }
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // delete chapter by id
  @DeleteMapping("/delete/{id}")
  public ResponseEntity<Chapter> deleteChapter(@PathVariable("id") long id) {
    try {
      pIChapterRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
