package com.devcamp.chapterarticle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.chapterarticle.model.Chapter;

@Repository
public interface IChapterRepository extends JpaRepository<Chapter, Long> {
  Chapter findById(long id);
}
