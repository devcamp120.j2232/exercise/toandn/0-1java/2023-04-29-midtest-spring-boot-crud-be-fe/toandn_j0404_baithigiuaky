"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// mảng chapter
var gListChapterObjects = [];
var gListArticleObjects = [];
var gArticleObject = {};
var gSTT = 0;
var gIdArticle;
var gIdChapter;

const gBASE_URL = "http://localhost:8080/article";

const ARRAY_NAME_COL = [
  "Stt",
  "articleCode",
  "articleName",
  "articleIntro",
  "articlePage",
  "action",
];
//khai báo các cột
const gCOL_NO = 0;
const gCOL_ARTICLE_CODE = 1;
const gCOL_ARTICLE_NAME = 2;
const gCOL_ARTICLE_INTRO = 3;
const gCOL_ARTICLE_PAGE = 4;
const gCOL_ACTION = 5;

// khởi tạo datatable  - chưa có data
var gDataTable = $("#article-table").DataTable({
  columns: [
    { data: ARRAY_NAME_COL[gCOL_NO] },
    { data: ARRAY_NAME_COL[gCOL_ARTICLE_CODE] },
    { data: ARRAY_NAME_COL[gCOL_ARTICLE_NAME] },
    { data: ARRAY_NAME_COL[gCOL_ARTICLE_INTRO] },
    { data: ARRAY_NAME_COL[gCOL_ARTICLE_PAGE] },
    { data: ARRAY_NAME_COL[gCOL_ACTION] },
  ],
  columnDefs: [
    //định nghĩa các cột cần hiện ra
    {
      targets: gCOL_NO, //cột STT
      className: "text-center text-primary",
      /*render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1; */
      render: function () {
        gSTT++;
        return gSTT;
      },
    },
    {
      targets: gCOL_ARTICLE_CODE,
      className: "text-center",
    },
    {
      targets: gCOL_ARTICLE_PAGE,
      className: "text-center",
    },
    {
      targets: gCOL_ACTION, //cột Action
      className: "text-center",
      defaultContent: `
                            <button id= "update-article"  <i title = 'DEATIL' class="fa-solid fa-file-circle-check text-info" style = "cursor:pointer; border: 1px white;"></i></button> &nbsp;
                            <button id= "delete-article" <i title = 'Delete' class="fa-solid fa-trash text-danger" style = "cursor:pointer;border: 1px white;"></i></button>`,
    },
  ],
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();

//gán sự kiện tạo chapter mới
$("#add-infor-chapter").on("click", function () {
  onBtnAddInforClick();
});
// gán sự kiện cho nút Confirm create (trên modal)
$("#btn-create-confirm").on("click", function () {
  onBtnConfirmCreateClick();
});
// gán sự kiện cho nút Cancel (trên modal)
$("#btn-create-close").on("click", function () {
  onBtnCloseCreateClick();
});

// 3 - U: gán sự kiện Update - khi nhấn nút edit
$("#article-table").on("click", "#update-article", function () {
  onBtnEditClick(this);
});
// gán sự kiện cho nút Confirm edit (trên modal)
$("#btn-update-confirm").on("click", function () {
  onBtnConfirmEditClick();
});

$("#article-table").on("click", "#delete-article", function () {
  onBtnDeleteClick(this);
});
// gán sự kiện cho nút Confirm edit (trên modal)
$("#btn-delete-confirm").on("click", function () {
  onBtnConfirmDeleteClick();
});
// gán sự kiện cho nút Cancel (trên modal)
$("#btn-delete-cancel").on("click", function () {
  onBtnCancelDeleteClick();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  "use strict";
  // lấy data từ server
  callApiToGetListData();
  callApiToGetListChapter();
}

//Get data chapter
function callApiToGetListChapter() {
  $.ajax({
    url: "http://localhost:8080/chapter/all",
    method: "GET",
    success: function (pObjRes) {
      gListChapterObjects = pObjRes;
      console.log(pObjRes);
      loadDataToChapterSelect(pObjRes);
    },
    error: function (pXhrObj) {
      console.log(pXhrObj.responseText);
    },
  });
}

var gChapterCreateSelect = $("#create-chapterSelect");
var gChapterUpdateSelect = $("#update-chapterSelected");

function loadDataToChapterSelect(pChapterList) {
  for (var i = 0; i < pChapterList.length; i++) {
    var bChapterOption = $("<option/>");
    bChapterOption.prop("value", pChapterList[i].id);
    bChapterOption.prop("text", pChapterList[i].chapterName);
    gChapterCreateSelect.append(bChapterOption);
  }
  for (var i = 0; i < pChapterList.length; i++) {
    var bChapterOption = $("<option/>");
    bChapterOption.prop("value", pChapterList[i].id);
    bChapterOption.prop("text", pChapterList[i].chapterName);
    gChapterUpdateSelect.append(bChapterOption);
  }
}

//hàm call API to get all article
function callApiToGetListData() {
  $.ajax({
    url: gBASE_URL + "/all",
    type: "GET",
    dataType: "json",
    success: function (responseObject) {
      gListArticleObjects = responseObject;
      console.log(responseObject);
      DisplayDataToTable(responseObject);
    },
    error: function (error) {
      console.assert(error.responseText);
    },
  });
}

//hàm xử lý sự kiện add infor article
function onBtnAddInforClick() {
  "use strict";
  console.log("Button add được click");
  $("#create-article-modal").modal("show");
}

// Hàm xử lý sự kiện khi icon close trên bảng đc click
function onBtnCloseCreateClick() {
  "use strict";
  $("#create-article-modal").modal("hide");
}

//Hàm xử lý khi nhấn nút confirm trên modal
function onBtnConfirmCreateClick() {
  // khai báo đối tượng
  var vObjData = {
    articleCode: "",
    articleName: "",
    articleIntro: "",
    articlePage: 0,
    chapterId: 0,
  };
  // B1: Thu thập dữ liệu
  getCreateData(vObjData);
  // B2: Validate update
  var vIsValidate = validateData(vObjData);
  if (vIsValidate) {
    // B3: update order
    $.ajax({
      url: gBASE_URL + "/create/" + vObjData.chapterId,
      type: "POST",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(vObjData),
      success: function (paramRes) {
        // B4: xử lý front-end
        handleCreateSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.responseText);
      },
    });
  }
}

// Hàm xử lý sự kiện khi icon edit trên bảng đc click
function onBtnEditClick(paramBtnEdit) {
  "use strict";
  console.log("Button edit được click");
  // hàm dựa vào button edit (edit or delete) xác định đc id
  var vTableRow = $(paramBtnEdit).parents("tr");
  var vRowData = gDataTable.row(vTableRow).data();
  gIdArticle = vRowData.id;
  getIdChapterByIdArticle();
  console.log("ID article lấy được: " + gIdArticle);
  getArticleById(gIdArticle);
}

//Hàm xử lý khi nhấn nút confirm edit trên modal
function onBtnConfirmEditClick() {
  // khai báo đối tượng
  var vObjData = {
    articleCode: "",
    articleName: "",
    articleIntro: "",
    articlePage: 0,
    chapterId: 0,
  };
  // B1: Thu thập dữ liệu
  getUpdateData(vObjData);
  // B2: Validate update
  var vIsValidate = validateData(vObjData);
  if (vIsValidate) {
    // B3: update
    $.ajax({
      url: gBASE_URL + "/update/" + vObjData.chapterId + "/" + gIdArticle,
      type: "PUT",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(vObjData),
      success: function (paramRes) {
        // B4: xử lý front-end
        handleUpdateSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.responseText);
      },
    });
  }
}

// Hàm xử lý sự kiện khi icon delete trên bảng đc click
function onBtnDeleteClick(paramBtnDelete) {
  "use strict";
  console.log("Button delete được click");
  // hàm dựa vào button detail (edit or delete) xác định đc id
  var vTableRow = $(paramBtnDelete).parents("tr");
  var vRowData = gDataTable.row(vTableRow).data();
  gIdArticle = vRowData.id;
  console.log("ID article lấy được: " + gIdArticle);
  // hiển thị modal lên
  $("#delete-article-modal").modal("show");
}

//sự kiện confirm delete order
function onBtnConfirmDeleteClick() {
  deleteArticleById(gIdArticle);
}
//cancel delete order
function onBtnCancelDeleteClick() {
  $("#delete-article-modal").modal("hide");
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm load dữ liệu vào data table
function DisplayDataToTable(paramData) {
  "use strict";
  gSTT = 0; // thiết lập lại stt
  $("#article-table").DataTable().clear(); // xóa dữ liệu
  $("#article-table").DataTable().rows.add(paramData); //thêm các dòng dữ liệu vào datatable
  $("#article-table").DataTable().draw(); //vẽ lại bảng
}

// hàm get article by id
function getArticleById(paramId) {
  $.ajax({
    url: gBASE_URL + "/detail/" + paramId,
    type: "GET",
    success: function (paramRes) {
      gArticleObject = paramRes;
      showDataToModalUpdate(paramRes);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    },
  });
  // hiển thị modal lên
  $("#update-article-modal").modal("show");
}

//lấy id chapter từ id article
function getIdChapterByIdArticle() {
  for (var i = 0; i < gListChapterObjects.length; i++) {
    var vArticle = gListChapterObjects[i].articles;
    for (var j = 0; j < vArticle.length; j++) {
      if (gIdArticle == vArticle[j].id) {
        gIdChapter = gListChapterObjects[i].id;
        console.log("ID chapter vừa lấy được: " + gIdChapter);
      }
    }
  }
}

// hàm show obj lên modal update
function showDataToModalUpdate(paramObj) {
  $("#update-articleCode").val(paramObj.articleCode);
  $("#update-articleName").val(paramObj.articleName);
  $("#update-articleIntro").val(paramObj.articleIntro);
  $("#update-articlePage").val(paramObj.articlePage);
  $("#update-chapterSelected").val(gIdChapter);
}

// hàm thu thập dữ liệu để create
function getCreateData(paramObj) {
  paramObj.articleCode = $("#create-articleCode").val().trim();
  paramObj.articleName = $("#create-articleName").val().trim();
  paramObj.articleIntro = $("#create-articleIntro").val().trim();
  paramObj.articlePage = parseInt($("#create-articlePage").val().trim());
  paramObj.chapterId = $("#create-chapterSelect").val();
}

// hàm thu thập dữ liệu để update article
function getUpdateData(paramObj) {
  paramObj.articleCode = $("#update-articleCode").val().trim();
  paramObj.articleName = $("#update-articleName").val().trim();
  paramObj.articleIntro = $("#update-articleIntro").val().trim();
  paramObj.articlePage = parseInt($("#update-articlePage").val().trim());
  paramObj.chapterId = $("#update-chapterSelected").val();
}

// hàm validate data
function validateData(paramObj) {
  if (paramObj.articleCode === "") {
    alert("Please fill article code!");
    return false;
  }
  for (var i = 0; i < gListArticleObjects.length; i++) {
    if (
      paramObj.articleCode == gListArticleObjects[i].articleCode &&
      gIdArticle != gListArticleObjects[i].id
    ) {
      alert("Article code already exists!");
      return false;
    }
  }
  if (paramObj.articleName === "") {
    alert("Please fill article name!");
    return false;
  }
  if (paramObj.articleIntro === "") {
    alert("Please fill article introduction!");
    return false;
  }
  if (paramObj.articlePage <= 0) {
    alert("Please fill article page!");
    return false;
  }
  if (paramObj.chapterId == 0) {
    alert("Please select chapter!");
    return false;
  }
  return true;
}

//hàm confirm create thành công
function handleCreateSuccess() {
  alert("Confirmed create data article successfully!");
  callApiToGetListData();
  resetCreateForm();
  $("#create-article-modal").modal("hide");
}

//hàm confirm edit thành công
function handleUpdateSuccess() {
  alert("Confirmed update data article successfully!");
  callApiToGetListData();
  resetEditForm();
  $("#update-article-modal").modal("hide");
}

// hàm xóa trắng form edit modal
function resetCreateForm() {
  $("#create-articleCode").val("");
  $("#create-articleName").val("");
  $("#create-articleIntro").val("");
  $("#create-articlePage").val("");
  $("#create-chapterSelect").val(0);
}

// hàm xóa trắng form edit modal
function resetEditForm() {
  $("#update-articleCode").val("");
  $("#update-articleName").val("");
  $("#update-articleIntro").val("");
  $("#update-articlePage").val("");
  $("#update-chapterSelected").val(0);
  location.reload();
}

// hàm get article by id
function deleteArticleById(paramId) {
  $.ajax({
    url: gBASE_URL + "/delete/" + paramId,
    type: "DELETE",
    async: false,
    success: function (paramRes) {
      console.log(paramRes);
      handleDeleteSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    },
  });
}

//hàm confirm delete thành công
function handleDeleteSuccess() {
  alert("Delete data article successfully!");
  callApiToGetListData();
  $("#delete-article-modal").modal("hide");
}
